# Docker Compose Demo

This project demonstrates the use of containerization technologies to manage and deploy a multi-architecture Docker application. It uses Docker together with a GitLab CI/CD pipeline for automating the build, test, and Kubernetes deployment processes.

## Overview

The application consists of the following Docker components:

- **Dockerfile**: This is used to build the Docker images.

- **Docker Compose**: Two docker compose files can be used to build/deploy the Docker image.

The application consists of the following Kubernetes components:

- **Namespace**: The `demo-app` namespace is created to isolate the application resources.

- **SSL Secret**: An SSL secret is created to encrypt the traffic to the application.

- **Deployment**: The application deployment is defined in `kubernetes/demo-deployment.yaml`. It describes the desired state for the application deployment, replication set and pods.

- **ClusterIP Service**: The ClusterIP service (defined in `kubernetes/demo-service-cluster-ip.yaml`) exposes the application within the Kubernetes cluster.

- **NodePort Service**: The NodePort service (defined in `kubernetes/demo-service-nodeport.yaml`) exposes the application outside the cluster on a static port.

- **Ingress**: The Ingress resource (defined in `kubernetes/demo-ingress.yaml`) defines how external HTTP/S traffic should be processed to reach the application.

The automated build and deployment uses the following components:

- **Shared Runners**: The application is built and tested using Gitlab's "Shared Runners".

- **On Premise Runner**: The Kubernetes components are deployed to the cluster using an on-premise runner. Using the on-premise runner allows the deployment to Kubernetes without exposing the cluster to the internet.

- **Container Scanning**: The container images are scanned and checked for any security vulnerabilities.

- **Application Testing**: The application is tested using a simple Curl check to ensure that the application starts, and responds.


## CI/CD Pipeline Stages

The build and test process is automated using GitLab CI/CD. The pipeline includes the following stages:

- **buildx**: Configure Buildx for building multi-architecture container images.
- **build**: Build multi-architecture container images using Buildx.
- **scan**: Scan the container image to check for security vulnerabilities.
- **test**: Run tests to ensure the application starts and responds.
- **deploy**: Automatically deploys the application and its components to Kubernetes.


## Deployment

The deployment process is also automated using GitLab CI/CD. The deployment stage includes the following steps:

1. **Use on-prem runner**: Utilize an on-premises runner to manage Kubernetes components.
2. **Update kubeconfig**: Decode and use the provided `$KUBE_CONFIG` to update the Kubernetes configuration.
3. **Create SSL secret**: Apply the SSL secret decoded from `$SSL_SECRET`.
4. **Update namespace**: If `kubernetes/demo-app-namespace.yaml` is updated, apply the changes.
5. **Update deployment**: If `kubernetes/demo-deployment.yaml` is updated, apply the changes.
6. **Update clusterip service**: If `kubernetes/demo-service-cluster-ip.yaml` is updated, apply the changes.
7. **Update nodeport service**: If `kubernetes/demo-service-nodeport.yaml` is updated, apply the changes.
8. **Update ingress**: If `kubernetes/demo-ingress.yaml` is updated, apply the changes.

**Note**: The update steps are conditional based on whether the respective manifest files have been updated in the Git commit.


## Dockerfile

The project includes a Dockerfile (`Dockerfile`) for building the Docker image.

### Prerequisites

Before using the Dockerfile, ensure that you have Docker installed on your system.

### Build and Run Locally

To build and run the Docker image locally:

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/olaf.terblanche/docker-compose-demo.git
   cd docker-compose-demo
   ```

2. Build the Docker image:

   ```bash
   docker build -t docker-demo-app .
   ```

3. Run the Docker container:

   ```bash
   docker run -p 8080:8080 my-demo-app
   ```


## Docker Compose

The project includes Docker Compose file(s) for orchestrating the local deployment of the application and its dependencies. The Docker Compose configuration(s) defines requirements to run the application in a multi-architechture environment.

### Prerequisites

Before using Docker Compose, ensure that you have Docker and Docker Compose installed on your system.

### Deploy with Docker Compose (Use existing image from Gitlab repository)

To deploy the application that pulls the existing image from the Gitlab repository using Docker Compose, follow these steps:

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/olaf.terblanche/docker-compose-demo.git
   cd docker-compose-demo
   ```

2. Deploy the application with Docker Compose:

   ```bash
   ## The -d flag runs the containers in the background.
   docker-compose -f demo-docker-compose-1.yaml up -d
   ```

3. Access the application:

   The application should now be accessible at http://localhost:8080

### Tear Down

To stop and remove the containers created by Docker Compose, run:

   ```bash
   docker-compose -f demo-docker-compose-1.yaml down
   ```

### Deploy with Docker Compose (Building the image locally)

To deploy the application that first builds the image locally using Docker Compose, follow these steps:

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/olaf.terblanche/docker-compose-demo.git
   cd docker-compose-demo
   ```

2. Deploy the application with Docker Compose:

   ```bash
   ## The -d flag runs the containers in the background.
   docker-compose -f demo-docker-compose-2.yaml up -d
   ```

3. Access the application:

   The application should now be accessible at http://localhost:8080

### Tear Down

To stop and remove the containers created by Docker Compose, run:

   ```bash
   docker-compose -f demo-docker-compose-2.yaml down
   ```

## Kuberenetes Deploy

The project includes Kuberenetes manifest file to allow for deployment to a Kubernetes clusters.

### Prerequisites

An existing Kubernetes cluster

### Deploy to existing Kuberenetes cluster

To deploy the application to a Kuberernetes cluster, follow these steps:

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/olaf.terblanche/docker-compose-demo.git
   cd docker-compose-demo
   
2. Update the CICD environment variables with your kube config and SSL secret

3. Deploy the application using the CICD pipeline:

   Update the manifest files, commit and push the changes



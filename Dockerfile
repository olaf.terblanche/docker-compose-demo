## Use an official httpd image based on Alpine
FROM httpd:alpine

## Set the working directory to /usr/local/apache2/htdocs/
WORKDIR /usr/local/apache2/htdocs/

## Copy the content of the demo-web-app/public directory to the container
COPY demo-web-app/public/ .

## Configure Apache to listen on port 8080
RUN sed -i 's/Listen 80/Listen 8080/g' /usr/local/apache2/conf/httpd.conf
RUN sed -i 's/VirtualHost \*:80/VirtualHost \*:8080/g' /usr/local/apache2/conf/httpd.conf

## Expose port 8080 for HTTP
EXPOSE 8080
